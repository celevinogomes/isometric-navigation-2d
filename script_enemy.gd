extends KinematicBody2D

export var movement_speed = 100.0

var distance = Vector2(0.0, 0.0)
var motion = Vector2(0.0, 0.0)
var movement_direction = Vector2(0.0, 0.0)
var navigation_2D = null
var path = []
var target = null

func _ready():
	navigation_2D = get_parent().get_parent()
	target = get_parent().get_node("Player")
	set_fixed_process(true)

func _fixed_process(delta):
	path = navigation_2D.get_simple_path(get_global_pos(), target.get_global_pos(), false)

	if (path.size() > 1):
		distance = path[1] - get_global_pos()
		movement_direction = distance.normalized()

		if (path.size() > 3):
			motion = delta * movement_direction * movement_speed
			motion = move(motion)

		update()

func _draw():
	if (path.size() > 0):
		for point in path:
			draw_circle(point - get_global_pos(), 5, Color(0, 1, 0))