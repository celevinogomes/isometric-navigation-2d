extends KinematicBody2D

export var movement_speed = 200.0

var motion = Vector2(0.0, 0.0)
var movement_direction = Vector2(0.0, 0.0)

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	movement_direction.x = 0.0
	movement_direction.y = 0.0

	if (Input.is_action_pressed("left")):
		movement_direction.x = -1.0

	if (Input.is_action_pressed("right")):
		movement_direction.x = 1.0

	if (Input.is_action_pressed("down")):
		movement_direction.y = 1.0

	if (Input.is_action_pressed("up")):
		movement_direction.y = -1.0

	movement_direction = movement_direction.normalized()

	if (movement_direction.length_squared() > 0.0):
		motion = delta * movement_direction * movement_speed
		motion = move(motion)

		if (is_colliding()):
			motion = get_collision_normal().slide(motion)
			motion = move(motion)